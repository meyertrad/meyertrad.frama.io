Title: Mes atouts
Slug: mes-atouts
Authors: meyertrad
Tags: allemand, anglais, traduction, traducteur, technique, mode, Vichy, Clermont-Ferrand
Description: Présentation de ma formation initiale et de mes formations continues
Summary: De ma formation initiale à mes formations continues

### Mes atouts
Je suis diplômée d'un Master de Traduction technique allemand/anglais de l'Université de Grenoble et j'ai travaillé chez Aérospatiale et en agence de traduction. 

J'ai toujours été au contact de l'industrie, en visitant de nombreuses entreprises et sites de production, et je continue de le faire aujourd'hui.

J'aime rencontrer les professionnels des secteurs dans lesquels je traduis, rien de plus efficace qu'une bonne communication avec le traducteur à qui l'on confie ses documents.

Je travaille dans l'échange avec d'autres traducteurs. Nous effectuons des relectures croisées, en accord avec le client final pour apporter un regard supplémentaire à la traduction.

Je suis régulièrement des formations dans mes domaines de travail ou les outils du traducteur. Je suis friande d'expos, notamment de mode.
Je participe également à des conférences en mécanique avec le CETIM.

### Formations

Voici quelques-unes des formations que j’ai suivies :

#### Mode
- L'Art Déco, Vichy (2015) 
- Coco Chanel : sa vie, ses parfums, la philosophie et la signature de la marque (2016) 
- Découvrir les parfums Dior : fabrication et terminologie du parfum (2017)
- Cours de couture avancée avec une styliste diplômée, Clermont-Ferrand (2012)

#### Rédaction et traduction
- Traduire le marketing : SFT, Clermont-Ferrand (2013) 
- Initiation à la traduction juridique : SFT, Clermont-Ferrand (2013)
- Ateliers d'écriture locaux (1er prix obtenu à l'un d'entre eux)
- Conventions de rédaction et terminologie applicables aux contrats et aux statuts de société : SFT, Lyon (2018)
- Rédiger sur le web : CCI, Clermont-Ferrand (2021)
