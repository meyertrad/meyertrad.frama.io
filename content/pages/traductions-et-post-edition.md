Title: Traductions et post-édition
Slug: traductions-et-post-edition
Authors: meyertrad
Tags: traduction, traducteur, technique, allemand, anglais, traduction automatique, Auvergne
Description: Traduction de vos documents rédigés en allemand ou en anglais
Summary: Traduction de vos documents rédigés en allemand ou en anglais

### Traductions et relecture
Je traduis tous types de documents : appels d'offres, brevets d'invention, rapports d'expertise, articles, catalogues de produits, sites, etc. dans les domaines **industriels** mais aussi en **architecture** et **BTP** et dans la **mode**. Je relis et corrige également des traductions dont vous n'auriez pas satisfaction.

Je définis au préalable avec vous la terminologie et le style à respecter tout au long de la traduction.

Je travaille avec un réseau de collègues traducteurs basés en Auvergne, tous membres de la Société Française des Traducteurs et respectant comme moi son code de déontologie professionnelle 

### Post-édition de traductions 
> Correction et adaptation des textes traduits automatiquement

Vous avez utilisé un outil de traduction automatique sur Internet mais souhaitez vous assurer qu'il n'y a pas **d'erreurs** ? Vous avez raison car ces outils, si performants soient-ils, ne savent pas lever les ambiguïtés du langage humain, "oublient" des mots et ne comprennent rien au second degré par exemple.

La **post-édition** consiste à faire relire un document traduit par un outil de traduction automatique par un *linguiste*. Ce spécialiste de la langue décèle toute erreur même subtile qui pourrait nuire à l'image de votre entreprise. Cette relecture permet également d'adapter le texte final au style de votre entreprise.
