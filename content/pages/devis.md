Title: Devis
Slug: devis
Authors: meyertrad
Tags: devis, traduction
Description: Vous trouverez comment me contacter et me confier vos documents pour un devis
Summary: N'hésitez pas à me contacter et à me confier vos documents pour un devis

Je réponds rapidement à vos demandes de devis. Envoyez-moi vos documents dans le format que vous aurez choisi. Le devis est établi par mot cible (mot traduit) pour une traduction ou selon le temps estimé pour une relecture ou une post-édition.

### Contact
* Tél. professionnel - +33 (0)9 72 52 39 97
* Tél. portable - +33 (0)6 60 77 14 60
* Courriel - meyer[POINT]trad[AT]ovh[POINT]fr 
