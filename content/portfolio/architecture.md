Title: Architecture et design
Date: 2021-04-20 16:00
Modified: 2021-04-20 16:00
Tags: architecture, design, BTP, bâtiment, Vichy, Auvergne, traduction, anglais, allemand
Slug: architecture
Lang: fr
Authors: meyertrad
Picture: ../../doc/jamie-hunt-s9q0L_4hIKI-unsplash.jpg
PictureCopyrights: Photo de Jamie Hunt sur Unsplash
PictureLink: https://unsplash.com/photos/s9q0L_4hIKI
Description: Tours, immeubles de bureaux, bâtiments utilitaires, l'architecture est partout et se traduit aussi
Summary: Tours, immeubles de bureaux, bâtiments utilitaires, l'architecture est partout et se traduit aussi

Ayant visité de nombreuses villes européennes, attirée par leur atmosphère et leur architecture, c'est tout naturellement que mes premières traductions se sont tournées vers l'architecture. Je traduis pour des architectes dans le cadre d'appel d'offres ou encore des brochures touristiques (description et histoire de bâtiments) à partir de mes langues de travail : anglais et allemand.

La ville de Vichy en Auvergne regorge de bâtiments de tous les styles et inspirations, sans oublier ceux qui sont les symboles du développement thermal. J'aime lever les yeux quand je m'y promène et regarder cette architecture qui m'entoure. 

Mais si j'aime l'extérieur, j'aime aussi l'intérieur. Je traduis également des sites et catalogues de meubles contemporains et design pour du mobilier de bureau. Bref, tout ce qui touche au design et qui est un plaisir pour les yeux.
