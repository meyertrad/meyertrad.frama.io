Title: Transports
Date: 2021-04-20 15:00
Modified: 2021-04-20 15:00
Tags: ferroviaire, technique, mécanique, traduction, automobile, aéronautique, spatial, anglais, allemand
Slug: transports
Lang: fr
Authors: meyertrad
Picture: ../../doc/sam-tindale-7mi0bZ7oWU8-unsplash.jpg
PictureCopyrights: Photo de Sam Tindale sur Unsplash
PictureLink: https://unsplash.com/photos/7mi0bZ7oWU8
Description: Des étoiles au plancher des vaches : je traduis dans le domaine automobile, aéronautique, spatial et ferroviaire
Summary: Des étoiles au plancher des vaches : je traduis dans le domaine automobile, aéronautique, spatial et ferroviaire

C'est au sein d'Aérospatiale que j'ai fait mes premières armes en terminologie et traduction pour le domaine spatial. De là, le pas vers l'aéronautique était vite franchi. Je traduis des manuels et des brevets pour ce secteur.

Mon penchant pour la technique et la mécanique allié à mon goût des voyages en train m'ont amenée tout naturellement à traduire tout ce qui touche au rail. 

Depuis 20 ans, je traduis de la documentation pour la SNCF, la Deutsche Bahn ou encore la CFF/SBB en Suisse. J'ai ainsi gagné de l'expérience et constitué une terminologie importante dans ce domaine.

Je réalise des traductions de documents techniques variés : manuels de maintenance des locomotives, des véhicules et des voies, brevets, manuels de formation des conducteurs de trains et des agents, documentation des systèmes d'aiguillage, etc.

Je traduis également de nombreux brevets et des manuels pour le secteur automobile et ses innovations technologiques.

J'ai une longue expérience de traduction de l'anglais et de l'allemand dans le domaine des transports.
