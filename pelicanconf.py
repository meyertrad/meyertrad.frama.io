#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'meyertrad'
TITLE = 'Odile Meyer Traductions'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Paris'
DEFAULT_DATE_FORMAT = '%d %B %Y'
DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# personnalisation
THEME = 'themes/html5-dopetrope'
STATIC_PATHS = ( "doc/", )

KEYWORDS = 'traduction, technique, allemand, anglais, Vichy, Auvergne, traduire'
DESCRIPTION = 'Traductrice depuis plus de 20 ans, je traduis de l\'allemand et de l\'anglais vers le français, ma langue maternelle. Je suis membre de la Société Française des Traducteurs et fais partie de la délégation régionale d\'Auvergne.'
HEADER_TITLE = 'Traductions générales et techniques'
HEADER_SUBTITLE = 'à Vichy (Auvergne)'
INTRO_TEXT = 'Traductrice depuis plus de 20 ans, je traduis de l\'<strong>allemand</strong> et de l\'<strong>anglais</strong> vers le français, ma langue maternelle. Je suis membre de la Société Française des <strong>Traducteurs</strong> et fais partie de la délégation régionale d\'Auvergne.'
BOOK_HEADER = 'Traducteur : un métier'
BOOK_TEXT = 'Je traduis tous types de documents techniques pour les entreprises, en  concertation avec le client pour la terminologie et le style qui lui sont propres.'
PEN_HEADER = 'Relecture de traductions'
PEN_TXT = 'Vous avez besoin de faire relire et éventuellement corriger un document que vous avez fait traduire ? Je reprends votre document point par point et le modifie conformément à vos attentes.'
USER_HEADER = 'Post-édition de traductions automatiques'
USER_TEXT = 'J\'apporte un regard "humain" sur la traduction effectuée par la machine afin d\'en corriger les erreurs subtiles.'
DEVIS_LINK = 'pages/devis.html'
DEVIS_LAB = 'Devis'
TRAD_LINK = 'pages/traductions-et-post-edition.html'
TRAD_LAB = 'Traduction et post-édition'
PORTFOLIO = 'Mes domaines'
DOMAINES = ('Industries et techniques / Mécanique / Ferroviaire',
            'Industries et techniques / Automobile / Spatial / Aéronautique',
            'Arts et littérature / Architecture / BTP / Design',
            'Tourisme / Mode / Loisirs créatifs')
COMBINAISONS = ('Allemand (AT,DE,LI,CH) -> Français (FR,CH)',
                'Anglais (US,UK,IR,AU) -> Français (FR)')
MES_ATOUTS = 'pages/mes-atouts.html'
CONTACT_IMAGE = 'theme/images/logo.png'
CONTACT_TEXT = 'Tél. professionnel - +33 (0)9 72 52 39 97<br/>Courriel - meyer[POINT]trad[AT]ovh[POINT]fr'
COPYRIGHT = 'Odile Meyer. Sauf mention contraire, le contenu est disponible sous licence <a href="https://creativecommons.org/licenses/by-sa/3.0/fr/" target="_blank">CC-BY-SA version 3.0 ou ultérieure</a>'
CODE = '<i class="icon brands fa-gitlab" aria-hidden="true"></i> Hébergé sur <a href="https://framagit.org/meyertrad/meyertrad.frama.io" target="_blank">Framagit</a>'
LOCALE = ('fr_FR.utf8', )
